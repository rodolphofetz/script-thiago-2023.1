#!/bin/bash

if [ -e $1 ]; then
	echo "ok"
else
	echo "erro!"
	exit 1
fi

echo " Digite 1 para as primeiras 10 linhas"
echo " Digite 2 para as ultimas 10 linhas"
echo " Digite 3 para o # de linhas"

read op

if [ $op = "1" ]; then
	head -10 $1
elif [ $op = "2" ]; then
	tail -10 $1
elif [ $op = "3" ]; then
	wc -l $1
else
	echo "errou!"
fi
