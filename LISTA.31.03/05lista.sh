#!/bin/bash

d=$(date +%y-%m-%d-%h)
x=/tmp/${d}
mkdir $x
cp * $x
zip -r a.zip $x
rm -rf $x
