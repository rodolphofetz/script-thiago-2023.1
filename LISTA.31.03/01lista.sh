#!/bin/bash

echo -e "\e[32m$x Frase motivacional 1: E sem saber que era impossível, foi lá e soube. \e[0m"
sleep 1

echo -e "\e[33m$x Frase motivacional 2: Desistir é para os fracos, o ideal é nem tentar. \e[0m"
sleep 1

echo " Fim ;*"
