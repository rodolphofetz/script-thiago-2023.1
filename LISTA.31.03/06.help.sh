#!/bin/bash

#Substituição de variáveis
nome="Sapecoso"
echo "Olá, $nome! Seu diretório atual é: $PWD"

#Substituição de shell
numero_arquivos=$(ls | wc -l)
echo "Existem $numero_arquivos arquivos neste diretório."

#Substituição aritmética
x=100
y=20
soma=$((x+y))
echo "A soma de $x e $y dá $soma."

sleep 2
echo "Meu nome é tchau!."
