#!/bin/bash

# Exemplos de criação de variáveis no bash

nome="Fulano"
echo "Digite o seu nome:"
read nome
declare -i idade=30
export MINHA_VARIAVEL="Algum valor aleatório"
data=$(date +%d/%m/%Y)
nome=$1

echo "Explicação sobre diferençca entre pedir explicitamente e recebelo como parâmentro:
# A diferença entre pedir explicitamente para o usuário digitar o valor de uma variável
# e recebê-la como parâmetro de linha de comando:
# Pedir para o usuário digitar interrompe a execução e espera a entrada do usuário,
# enquanto receber como parâmentro não interrompe a execução.
"
