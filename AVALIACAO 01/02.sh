#!/bin/bash

echo "Qual o primeiro diretório?"
read dir1

echo "Qual o segundo diretório?"
read dir2

echo "Qual o terceiro diretório?"
read dir3

echo "Aqui está a quantidade de arquivos .txt em $dir1: "
find "$dir1" -name "*.txt" | wc 01

echo "Aqui está a quantidade de arquivos .png em $dir1: "
find "$dir1" -name "*.png" | wc -1

echo "Aqui está a quantidade de arquivos .doc em $dir1: "
find "$dir1" -name "*.doc" | wc -1

echo "Aqui está a quantidade de arquivos .txt em $dir2: "
find "$dir2" -name "*.txt" | wc 01

echo "Aqui está a quantidade de arquivos .png em $dir2: "
find "$dir2" -name "*.png" | wc -1

echo "Aqui está a quantidade de arquivos .doc em $dir2: "
find "$dir2" -name "*.doc" | wc -1

echo "Aqui está a quantidade de arquivos .txt em $dir3: "
find "$dir3" -name "*.txt" | wc 01

echo "Aqui está a quantidade de arquivos .png em $dir3: "
find "$dir3" -name "*.png" | wc -1

echo "Aqui está a quantidade de arquivos .doc em $dir3: "
find "$dir3" -name "*.doc" | wc -1
