#!/bin/bash

echo "Variáveis automáticas do Bash:"

echo "========================="
echo "PID: $$"
echo "Número da última execução de background: $!"
echo "Status de saída do último comando: $?"
echo "Número de argumentos passados para o script: $#"
echo "Lista de argumentos passados para o script: $@"
echo "Nome do script: $0"
echo "Nome do usuário que executou o script: $USER"
echo "Nome do shell em uso: $SHELL"
echo "Versão do Bash: $BASH_VERSION"
echo "Arquitetura do sistema: $HOSTTYPE"
echo "========================="
