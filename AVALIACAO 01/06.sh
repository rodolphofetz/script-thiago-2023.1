#!/bin/bash

for dir in "$@"; do
	mkdir "$dir"
	echo "$dir" > "$dir/README.md"
done
