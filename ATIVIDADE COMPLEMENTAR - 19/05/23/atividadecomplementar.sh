#!/bin/bash

conteudo=$(cat "$1") 
linhas=$(wc -l < "$1") 
 
[[ $(wc -l < "$2") -gt $linhas ]] && { conteudo=$(cat "$2"); linhas=$(wc -l < "$2"); } 
[[ $(wc -l < "$3") -gt $linhas ]] && { conteudo=$(cat "$3"); linhas=$(wc -l < "$3"); } 
[[ $(wc -l < "$4") -gt $linhas ]] && { conteudo=$(cat "$4"); linhas=$(wc -l < "$4"); } 
 
echo "$conteudo"
