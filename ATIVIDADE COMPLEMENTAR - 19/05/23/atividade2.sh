#!/bin/bash
conteudo="" 
linhas=0 

for arquivo in "$1" "$2" "$3" "$4"; do 
	num_linhas=$(wc -l < "$arquivo") 
	if [ "$num_linhas" -gt "$linhas" ]; then 
		conteudo=$(cat "$arquivo") 
		linhas="$num_linhas" 
	fi 
	done 

echo "$conteudo"
