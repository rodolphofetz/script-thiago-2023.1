#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Número não informado."
  exit 1
fi

numero=$1
soma=0

for ((i=1; i<=numero; i++))
do
  soma=$((soma + i))
done

echo "A soma de todos os números de 1 até $numero é: $soma"
