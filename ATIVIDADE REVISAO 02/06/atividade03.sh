#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Nome do arquivo não fornecido."
  exit 1
fi

if [ ! -f "$1" ]; then
  echo "Arquivo não encontrado."
  exit 1
fi

while read -r site
do
  diretorio=$(echo "$site" | sed 's#https\?://##;s#/$##')
  
  mkdir -p "$diretorio"
  wget -P "$diretorio" -r -np -k "$site"
done < "$1"
