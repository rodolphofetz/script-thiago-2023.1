#!/bin/bash

for arquivo in /etc/* /tmp/*
do
  if [ -d "$arquivo" ]; then
    tipo="Diretório"
  elif [ -f "$arquivo" ]; then
    tipo="Arquivo"
  elif [ -L "$arquivo" ]; then
    tipo="Link"
  elif [ -x "$arquivo" ]; then
    tipo="Executável"
  else
    tipo="Desconhecido"
  fi
  
  echo "Arquivo: $arquivo"
  echo "Tipo: $tipo"
  echo
done

