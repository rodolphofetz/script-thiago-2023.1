#!/bin/bash

# Salva o valor original da variável PS1
PS1_ORIGINAL="$PS1"

# Define as opções de personalização
opcao_1="\[\033[0;32m\]\u@\h \[\033[0;34m\]\w\[\033[0m\] $ "
opcao_2="\[\033[0;31m\]\t \[\033[0m\]$ "
opcao_3="\[\033[0;34m\]\u@\h \[\033[0;36m\]\w\[\033[0m\] $ "
opcao_4="\[\033[0;33m\]\u \[\033[0m\]\t $ "

# Exibe as opções de personalização disponíveis
echo "Opções de personalização:"
echo "  1) Prompt verde com nome de usuário e diretório atual"
echo "  2) Prompt vermelho com data e hora"
echo "  3) Prompt azul com nome de usuário, diretório atual e hostname"
echo "  4) Prompt amarelo com nome de usuário e hora"
echo "  5) Retornar para o prompt original"

# Lê a opção escolhida pelo usuário
read opcao

# Personaliza o prompt de acordo com a opção escolhida
PS1="${!opcao}"
if [ "$opcao" != "5" ]; then
    echo "Prompt personalizado!"
else
    PS1="$PS1_ORIGINAL"
    echo "Prompt original restaurado!"
fi
