#!/bin/bash

result=$(find . /tmp /etc -name "$1" 2>/dev/null)

[ -n "$result" ] && echo "O arquivo $1 foi encontrado em: $result" || echo "O arquivo $1 não foi encontrado em nenhum dos diretórios especificados."

