#!/bin/bash

read -p "Insira o primeiro valor: " valor1
read -p "Insira o segundo valor: " valor2

soma=$((valor1 + valor2))
subtracao=$((valor1 - valor2))
multiplicacao=$((valor1 * valor2))
divisao=$(echo "scale=2; $valor1 / $valor2" | bc)
raiz_quadrada=$(echo "scale=2; sqrt($valor1)" | bc)

echo "A soma deu: $soma"
echo "A subtração deu: $subtracao"
echo "A multiplicação deu: $multiplicacao"
echo "A divisão deu: $divisao"
echo "A raiz quadrada de $valor1 é: $raiz_quadrada"

