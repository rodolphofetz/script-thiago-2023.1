#!/bin/bash

quartas=$(date -d "next Wednesday" +"%d/%m/%Y")
quartas="$quartas $(date -d "next Wednesday + 7 days" +"%d/%m/%Y")"
quartas="$quartas $(date -d "next Wednesday + 14 days" +"%d/%m/%Y")"
quartas="$quartas $(date -d "next Wednesday + 21 days" +"%d/%m/%Y")"

for data in $quartas; do
  # Cria o diretório backup com o nome da data
  mkdir "backup$data"
done

echo "As próximas quartas-feiras são:"
echo $quartas

