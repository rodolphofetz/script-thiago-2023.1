#!/bin/bash

# Solicitar tamanho da senha
tamanho=$(yad --entry --title "Gerador de Senhas" --text "Digite o tamanho da senha:" --width=300 --center --on-top)

# Verificar se o usuário cancelou
if [ $? -ne 0 ]; then
    exit 0
fi

# Definir caracteres permitidos
caracteres="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%*"

# Gerar senhas aleatórias
senhas=$(for i in $(seq 1 3); do echo $(head /dev/urandom | tr -dc "$caracteres" | head -c "$tamanho"); done)

# Exibir senhas geradas
yad --title "Senhas Geradas" --text "As seguintes senhas foram geradas: $senhas" \
    --info --width=300 --height=200 --center --on-top --button="Fechar":0 \
