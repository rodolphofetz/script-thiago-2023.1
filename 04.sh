#!/bin/bash

echo "Informações sobre o hardware do computador:"

echo "========================================"

echo "Processador:"
lscpu | grep -E 'Model name|Architecture'

echo "Memória RAM:"
free -h

echo "Dispositivos de armazenamento:"
lsblk

echo "Placa de vídeo:"
lspci | grep VGA

echo "Placa de rede:"
lspci | grep Ethernet

echo "Sistema Operacional:"
lsb_release -a

