#!/bin/bash

#IFPB
#CST em Redes de Computadores
#Projeto da Disciplina de Programação de Script - 2023.1
#Aluno: Rodolpho Costa Lisboa Netto - 20212380009
#Professor: Thiago Gouveia
#####################################################

# Verificar se o server SSH está instalado
function check_ssh_server() {
    if ssh -V >/dev/null 2>&1; then
        echo "Servidor SSH está instalado."
    else
        echo "Servidor SSH não está instalado."
    fi
}

# Seletor de arquivo
function select_file() {
    local file=$(yad --file --title="Selecione um arquivo" --width=500 --height=300)
    echo "$file"
}

# Exibir lista de arquivos no diretório
function list_files() {
    local directory="$1"
    ls "$directory"
}

# Copiar arquivo
function copy_file() {
    local source="$1"
    local destination="$2"
    scp "$source" "$destination"
}

# Exibir o menu principal
function main_menu() {
    local choices=("Copiar arquivo do cliente para o servidor" \
                   "Copiar arquivo do servidor para o cliente" \
                   "Ver lista de arquivos do cliente" \
                   "Ver lista de arquivos do servidor" \
                   "Salvar IP/Host" \
                   "Verificar se o servidor SSH está instalado")

    local choice=$(yad --title="Menu Principal" --width=300 --height=300 --button="gtk-cancel:1" --list --column="Opções" 
"${choices[@]}")

    case "$choice" in
        "Copiar arquivo do cliente para o servidor")
            local source_file=$(select_file)
            local destination=$(yad --file --directory --title="Selecione o destino" --width=500 --height=300)
            copy_file "$source_file" "$destination"
            ;;
        "Copiar arquivo do servidor para o cliente")
            local source=$(yad --file --directory --title="Selecione a origem" --width=500 --height=300)
            local destination=$(select_file)
            copy_file "$source" "$destination"
            ;;
        "Ver lista de arquivos do cliente")
            local client_dir=$(yad --file --directory --title="Selecione o diretório do cliente" --width=500 --height=300)
            list_files "$client_dir"
            ;;
        "Ver lista de arquivos do servidor")
            local server_dir=$(yad --file --directory --title="Selecione o diretório do servidor" --width=500 --height=300)
            list_files "$server_dir"
            ;;
        "Salvar IP/Host")
            local ip=$(yad --title="Salvar IP/Host" --text="Digite o IP/Host:" --entry)
            echo "$ip" >> saved_ips.txt
            ;;
        "Verificar se o servidor SSH está instalado")
            check_ssh_server
            ;;
        *)
            exit 0
            ;;
    esac
}

# Executa o menu.
main_menu

